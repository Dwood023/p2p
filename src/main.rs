use std::io::prelude::*;
use std::io::Write;
use std::io;
use std::net::{TcpListener, TcpStream};
use std::str;

const PORT: u32 = 5005;
const PEER: &str = "192.168.1.70";

fn pong(stream: &mut TcpStream) {
    let mut buff = &mut [0 as u8];

    while stream.read(buff).unwrap() != 0 {
        print!("{}", str::from_utf8(buff).unwrap());
        std::io::stdout().flush().unwrap();
    }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~IO~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
fn input(prompt: &str) -> String {
    print!("{}", prompt);
    std::io::stdout().flush().unwrap();
    let mut user_input = String::new();
    io::stdin().read_line(&mut user_input)
        .expect("Failed to read line");

    if user_input.ends_with('\n') {
        user_input.pop();
    }

    user_input
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~IO~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


// =========================== States =============================

// Returns an integer representing the first option the user is presented with
// These options should be moved into some higher scope at some point
fn top_level_option() -> i32 {
    // TODO: The options should be stored in a map with their execution method.
    // perhaps function pointers or functors?

    const FIRST_OPTION: i32 = 1;
    const LAST_OPTION: i32 = 2;

    print!("What would you like to do? \n
              1) Listen for connections.\n
              2) Instigate a connection. \n");
    std::io::stdout().flush().unwrap();

    loop {
        let response = input("Select an option as a number");
        match response.parse::<i32>() {
            Ok(i) => {
                if i >= FIRST_OPTION  &&  i <= LAST_OPTION {
                    return i;
                }
                else {
                    print!("{} is not a valid option. Please enter a number between\
                     {} and {}", i, FIRST_OPTION, LAST_OPTION);
                    std::io::stdout().flush().unwrap();
                }
            },
            Err(e) => {
                print!("Please enter a single number to select an option");
                std::io::stdout().flush().unwrap();
            }
        }
    }
}


fn listen() -> std::io::Result<()> {
    print!("Listening for connections. \n");
    std::io::stdout().flush().unwrap();
    let listener = TcpListener::bind("0.0.0.0:7777")?;
    for stream in listener.incoming() {
        pong(&mut stream?);
    }
    Ok(())
}


fn connect() -> std::io::Result<()> {
    print!("Instigating connection \n");
    std::io::stdout().flush().unwrap();
    // TODO: Make address user-specifiable

    let mut stream = match TcpStream::connect("192.168.1.70:7777") {
        Ok(result) => {
            print!("Connection successful. \n");
            std::io::stdout().flush().unwrap();
            result
        },
        Err(e) => {
            print!("Connection failed with error: {}. \n", e);
            return Err(e);
        },
    };

    let my_str : &str = "Seagull";
    stream.write(&my_str.as_bytes())?;
    Ok(())
}

// =========================== ~States ============================

fn main() -> std::io::Result<()>{
    print!("Welcome to seagull chat!");
    std::io::stdout().flush().unwrap();

    loop {
        match top_level_option() {
            1 => {
                listen();
            }
            2 => {
                connect();
            }
            _ =>{
                print!("invalid option \n");
                std::io::stdout().flush().unwrap();
            }
        }
    }

    Ok(())
}
